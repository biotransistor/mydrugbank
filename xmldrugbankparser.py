"""
# file : xmldrugnbankparser.py 
# 
# history: 
#     author: bue
#     date: 2014-01-13 
#     license: >= GPL3 
#     language: Python 3.3.3
#     description: http://drugbank.ca drug bank xml database parser 
#
#  version:
#     0.1-0 2014-01-16 handeDrugbank function parses and extract information from the official drugbank.xml to bild an igv track  
#     0.1-1 2014-01-22 xmldrugbank2txtrdb handeDrugbank taco for a glue interface naming convention compatible function
#
"""
import sys # error exit
from lxml import etree
import pygluedev.glue as gl  # bue 20140219: old glue library version


# level 2
def handleAction(s_actiontype, drugelement):
    """
    # internal methode
    """
    # set output variable
    ll_out = []
    # processing
    if (s_actiontype == '{http://drugbank.ca}targets'):
        s_actiontypee = 'target'
    elif (s_actiontype == '{http://drugbank.ca}enzymes'):
        s_actiontypee = 'enzyme'
    elif (s_actiontype == '{http://drugbank.ca}transporters'):
        s_actiontypee = 'transporter'
    elif (s_actiontype == '{http://drugbank.ca}carriers'):
        s_actiontypee = 'carrier'
    else:
        s_actiontypee = ("Error: unknown action type : "+s_actiontype)
    actions = drugelement.find(s_actiontype)
    actionelements = actions.getchildren()
    for actionelement in actionelements:
        # set output variable
        s_partner = None
        s_position =None
        l_action = []
        s_knowenaction = None
        # processing 
        s_partner = actionelement.get('partner')
        s_position = actionelement.get('position')
        #print(s_partner, s_position)
        if s_actiontype == '{http://drugbank.ca}targets': 
            knownaction = actionelement.find('{http://drugbank.ca}known-action')  # only by target
            s_knowenaction = knownaction.text  # only by target
            #print(s_knowenaction)
        actions = actionelement.find('{http://drugbank.ca}actions')
        for action in actions: 
            l_action.append(action.text)
            #print(action.tag, action.text)
        # output
        for s_action in l_action:
            ll_out.append([s_actiontypee,s_partner,s_position,s_action,s_knowenaction])
    #print(str(ll_out))
    return(ll_out)


# level 1 
def handleDrug(o_tree): 
    """
    # internal methode
    """
    print("\nhandle drugs...")
    lt_output = []
    # for each drug
    for drugbankid  in o_tree.getiterator('{http://drugbank.ca}drugbank-id'):  # get each drug id
        # drug 
        d_drug = {}
        drugelement = drugbankid.getparent()  # get each drug element by drug-id   
        id = drugelement.find('{http://drugbank.ca}drugbank-id')
        d_drug.update({'id':id.text}) 
        #print (id.tag, id.text)
        name = drugelement.find('{http://drugbank.ca}name') 
        d_drug.update({'name':name.text}) 
        #print (name.tag, name.text)
        cas = drugelement.find('{http://drugbank.ca}cas-number')
        d_drug.update({'cas':cas.text}) 
        #print (cas.tag, cas.text)   
        # drug group
        groups = drugelement.find('{http://drugbank.ca}groups')
        groupelements = groups.getchildren()
        l_group = []
        for groupelement in groupelements:
            l_group.append(groupelement.text)
            #print (groupelement.tag, groupelement.text)
        # drug
        ll_drug = []
        for s_group in l_group: 
            ll_drug.append([ d_drug['id'],d_drug['name'],d_drug['cas'],s_group ])
        #print(ll_drug) 
        # drug target
        s_action  = '{http://drugbank.ca}targets'
        ll_target = handleAction(s_action, drugelement)
        #print(ll_target) 
        # drug enzyme
        s_action  = '{http://drugbank.ca}enzymes'
        ll_enzyme = handleAction(s_action, drugelement)
        #print(ll_enzyme)
        # drug transporter 
        s_action  = '{http://drugbank.ca}transporters'
        ll_transporter = handleAction(s_action, drugelement)
        #print(ll_transporter) 
        # drug carrier
        s_action  = '{http://drugbank.ca}carriers'
        ll_carrier = handleAction(s_action, drugelement)
        #print(ll_carrier) 
        # lines
        lt_out = []
        if ( len(ll_target) == 0 and len(ll_enzyme) == 0 and  len(ll_transporter) == 0 and len(ll_carrier) == 0 ):
            for l_drug in ll_drug: 
                lt_out.append(tuple(l_drug + [None,None,None,None,None]))
        else:
            for l_drug in ll_drug: 
                for l_target in ll_target:
                    lt_out.append(tuple(l_drug + l_target))

            for l_drug in ll_drug: 
                for l_enzyme in ll_enzyme:
                    lt_out.append(tuple(l_drug + l_enzyme))

            for l_drug in ll_drug: 
                for l_transporter in ll_transporter:
                    lt_out.append(tuple(l_drug + l_transporter))

            for l_drug in ll_drug: 
                for l_carrier in ll_carrier:
                    lt_out.append(tuple(l_drug + l_carrier))

        lt_output = lt_output + lt_out
        #print(lt_out) 
        print(id.text, name.tag, name.text)
    #print(str(lt_output))
    return(lt_output)


# level 1 
def handlePartner(o_tree):
    """
    # internal methode
    """
    print("\nhandle partner...")
    partners = o_tree.find('{http://drugbank.ca}partners')  # locate partners section in the xml
    dt_partner = { None : (None, None, None, None) } 
    # for each partner element in partners section
    for partnerelement in partners: 
        # id 
        d_partner = {} 
        partnerid = partnerelement.get('id')
        d_partner.update({'id':partnerid})
        #print(partnerid)
        # name
        name = partnerelement.find('{http://drugbank.ca}name')
        d_partner.update({'name':name.text})
        #print(name.tag, name.text)
        # gene
        gene = partnerelement.find('{http://drugbank.ca}gene-name')
        d_partner.update({'gene':gene.text})
        #print(gene.tag, gene.text)
        if gene.text != None: 
            # species
            species = partnerelement.find('{http://drugbank.ca}species')
            #d_partner.update({'species':species.text})
            #print(species.tag, species.text)
            #print(partnerid, gene.tag+'#'+str(gene.text)+'#'+str(species.text)+'#')
            if species.text != None: 
                # category
                specieselements = species.getchildren()
                category = specieselements[0]  # bue 20140114: postion 0 hopefully always stays category!
                d_partner.update({'category':category.text})
                #print(category.tag, category.text)
            else: 
                d_partner.update({'category':None})  # none human partners ?
        else: 
          d_partner.update({'category':None})  # none gene partners
        # line 
        dt_partner.update( { d_partner['id'] : tuple([d_partner['id'], d_partner['name'], d_partner['gene'], d_partner['category']]) } )
        print(partnerid, gene.tag, gene.text)
    # output
    #print(dt_partner)
    return(dt_partner)


# level 0
def handleDrugbank(s_inputfile='drugbank00.xml'):
    """
    handleDrugbank - to extract information from the drugbank.xml into glue interface compatible data types
    input: 
        s_inputfile : file path and name of the drugbank file. default drugbank00.xml for test purpose.
            downold the latest version of drugbank.xml from http://www.drugbank.ca/ !
    output: 
        lt_matrix : list of tuple 
        t_xaxis : python tuple with column labels
    """
    o_tree = etree.parse(s_inputfile)  # load drugbank xml file into lxml etree
    lt_drug = handleDrug(o_tree)
    dt_partner = handlePartner(o_tree)
    i_total = len(lt_drug) 
    # fuse drug and partner to one list of tuples
    print("\nfuse drug and partner tuples ...")
    t_xaxis = ('drug_id','drug_name','drug_cas','drug_group','target_group','traget_id','target_position','drug_action','direct_drug_action','target_gene_name','taget_gene_id','target_species')
    print(t_xaxis)
    i_count = 1
    lt_matrix = []
    for t_drug in lt_drug:
        s_partnerid = t_drug[5]  # partner at position 5 of each drug tuple
        t_partner = dt_partner[s_partnerid]  
        t_out = (t_drug[0], t_drug[1], t_drug[2], t_drug[3], t_drug[4], t_drug[5], t_drug[6], t_drug[7], t_drug[8], t_partner[1], t_partner[2], t_partner[3])
        lt_matrix.append(t_out) 
        print(str(i_count)+'/'+str(i_total), t_out[0], t_out[1],' >>< ', t_out[10], t_out[11])
        i_count += 1
    return(lt_matrix, t_xaxis)


# level glue
def xmldrugbank2txtrdb(s_inputfile='drugbank.xml', s_outputfile='drugbank'):
    """
    xmldrugbank2txtrdb - glue interface nameing convention compatible taco for the handleDrugbank function
    input:
        s_inputfile : file path and name of the drugbank.xml file. downloaded form http://www.drugbank.ca
        s_outputfile :  output file name without file extensoion. default drugbank 
    output:  
        b_ok: boolean ok output
        txt rdb: rdb type text file in tab delimited text format. defaulr drugbank_rdb.txt
    """
    b_ok = False
    print('i xmldrugbank2txtrdb s_inputfile :', s_inputfile)
    print('i xmldrugbank2txtrdb s_outputfile :', s_outputfile)
    (lt_matrix, t_xaxis) = handleDrugbank(s_inputfile=s_inputfile)
    b_ok = gl.pylt2txtrdb(s_outputfile=s_outputfile, lt_matrix=lt_matrix, t_xaxis=t_xaxis)
    print('o xmldrugbank2txtrdb b_ok :', b_ok)
    return(b_ok)


def txtdrugbank2txtrdbgene(s_inputfile='drugbank_rdb.txt', s_outputfile='drugbank'): 
    """
    txtdrugbank2txtrdb - drug_group gene_id key view at drugbank
    input:
        s_inputfile : file path and name of the drugbank_rdb.txt file. generated by xmldrugbank2txtrdb
        s_outputfile :  output file name without file extensoion. default drugbankgene 
    output:  
        b_ok: boolean ok output
        txt rdb: rdb type text files in tab delimited text format. default drugbank will lead to drugbank_approved_rdb.txt
    """
    b_ok = False
    print('i txtdrugbank2txtrdb s_inputfile :', s_inputfile)
    print('i txtdrugbank2txtrdb s_outputfile :', s_outputfile)
    # read form text file
    tivtsvivs_value = ('drug_name',)
    tivtsvivs_key = ('drug_group','taget_gene_id',)  # one track per drug group, dont care about: target_species, drug action
    (dt_matrix, ts_xaxis, ts_xaxis_value, ts_xaxis_key, s_xaxis_primarykey, ts_yaxis, dt_matrix_count) = gl.txtrdb2pydt(s_matrix=s_inputfile, tivtsvivs_value=tivtsvivs_value, tivtsvivs_key=tivtsvivs_key, tivtsvivs_primarykey=tivtsvivs_key, ivs_labelrow=1, b_dtl=True)
    ls_key = dt_matrix.keys()
    # empty dictionary 
    dt_approved = {}
    dt_experimental = {}
    dt_illicit = {}
    dt_nutraceutical = {}
    dt_withdrawn = {}
    # poplulate dictionaries
    for s_key in ls_key :
        s_druggroup = dt_matrix[s_key][0][0]
        s_gene = dt_matrix[s_key][1][0]
        ls_drugs = dt_matrix[s_key][2]
        i_drugs = len(ls_drugs)
        if (s_druggroup == 'approved'):
            if (len(s_gene) != 0):
                dt_approved.update({s_gene : (ls_drugs,i_drugs)})
        elif (s_druggroup == 'experimental'):
            if (len(s_gene) != 0):
                dt_experimental.update({s_gene : (ls_drugs,i_drugs)})
        elif (s_druggroup == 'illicit'):
            if (len(s_gene) != 0):
                dt_illicit.update({s_gene : (ls_drugs,i_drugs)})
        elif (s_druggroup == 'nutraceutical'):
            if (len(s_gene) != 0):
                dt_nutraceutical.update({s_gene : (ls_drugs,i_drugs)})
        elif (s_druggroup == 'withdrawn'):
            if (len(s_gene) != 0):
                dt_withdrawn.update({s_gene : (ls_drugs,i_drugs)})
        else :
            sys.exit('Error : unknown drug group :', s_druggroup) 
    # write to file
    t_xaxis = ('gene_id','feature','value',)
    if (len(dt_approved) != 0):  
        b_approved = gl.pydt2txtrdb(s_outputfile=s_outputfile+'_approved', dt_matrix=dt_approved, t_xaxis=t_xaxis)
    if (len(dt_experimental) != 0): 
        b_experimental = gl.pydt2txtrdb(s_outputfile=s_outputfile+'_experimental', dt_matrix=dt_experimental, t_xaxis=t_xaxis)
    if (len(dt_illicit) != 0): 
        b_illicit = gl.pydt2txtrdb(s_outputfile=s_outputfile+'_illicit', dt_matrix=dt_illicit, t_xaxis=t_xaxis)
    if (len(dt_nutraceutical) != 0): 
        b_nutraceutical = gl.pydt2txtrdb(s_outputfile=s_outputfile+'_nutraceutical', dt_matrix=dt_nutraceutical, t_xaxis=t_xaxis)
    if (len(dt_withdrawn) != 0): 
        b_withdrawn = gl.pydt2txtrdb(s_outputfile=s_outputfile+'_withdrawn', dt_matrix=dt_withdrawn, t_xaxis=t_xaxis)
    # output
    b_ok = True
    print('o txtdrugbank2txtrdb b_ok :', b_ok)
    return(b_ok)




# module self test
if __name__=='__main__':
    print('*** self test xml drugbank 2 txt rdb ***')
    s_inputfile = 'drugbank.xml'  # for real drugbank version3
    s_outputfile = 'drugbank3'  # for real drugbank version3
    s_inputfile = 'drugbank03.xml'  # for test reason
    s_outputfile = 'selftest_drugbank3'  # for test reason  
    b_ok = xmldrugbank2txtrdb(s_inputfile=s_inputfile, s_outputfile=s_outputfile)

    print('\n*** self txt drugbank 2 txt rdb gene ***')
    s_inputfile = 'drugbank3_rdb.txt'  # for real drugbank version3
    s_outputfile = 'drugbank3'  # for real drugbank version3
    s_inputfile = 'selftest_drugbank3_rdb.txt'  # for test reason
    s_outputfile = 'selftest_drugbank3'  # for test reason  
    b_ok = txtdrugbank2txtrdbgene(s_inputfile=s_inputfile, s_outputfile=s_outputfile)
